#!/bin/bash
set -xe

#modules=()
#modules=(cod_activity_streams cod_featured cod_notices cod_profile_social cod_user_profile_pages cod_body cod_follow cod_notify cod_q_a cod_utility_links cod_bw cod_groups cod_pages cod_radioactivity cod_wikis cod_content_moderation cod_like cod_polls cod_search cod_wysiwyg cod_documents cod_location cod_posts cod_site_homepage cod_events cod_misc cod_profile_base cod_topics cod_social_sharing cod_trusted_contacts)
#themes=(cod_origins)

pull_git() {
    cd $BUILD_PATH/cod_profile
    if [[ -n $RESET ]]; then
      git reset --hard HEAD
    fi
    git pull origin 7.x-1.x

    for i in `cat $BUILD_PATH/repos.txt`; do
      echo $i
      cd "${BUILD_PATH}/repos/${i}"
      if [[ -n $RESET ]]; then
        git reset --hard HEAD
      fi
      git fetch origin
      git pull origin
    done
}

release_notes() {
  rm -rf rn.txt
  pull_git $BUILD_PATH
  OUTPUT="<h2>Release Notes for $RELEASE</h2>"
  cd $BUILD_PATH/cod_profile
  OUTPUT="$OUTPUT <h3>Drupal COD:</h3> `drush rn --date $FROM_DATE $TO_DATE`"

  ## old repos. don't use this anymore
  # cd $BUILD_PATH/repos/modules
  # for i in "${modules[@]}"; do
  #  echo $i
  #  cd $i
  #  RN=`drush rn --date $FROM_DATE $TO_DATE`
  #  if [[ -n $RN ]]; then
  #    OUTPUT="$OUTPUT <h3>$i:</h3> $RN"
  #  fi
  #  cd ..
  #done
  #cd $BUILD_PATH/repos/themes/cod_origins
  #RN=`drush rn --date $FROM_DATE $TO_DATE`
  #if [[ -n $RN ]]; then
  #  OUTPUT="$OUTPUT <h3>cod_origins:</h3> $RN"
  #fi

  echo $OUTPUT >> $BUILD_PATH/rn.txt
  echo "Release notes for $RELEASE created at $BUILD_PATH/rn.txt"
}

build_distro() {
  [[ $BUILD_PATH && ${BUILD_PATH-x} ]] || BUILD_PATH="."
  if [[ ${BUILD_PATH} == '.' ]]; then
    BUILD_PATH=$(pwd)
  fi
  if [[ -d $BUILD_PATH ]]; then
      cd $BUILD_PATH
      #backup the sites directory
      if [[ -d htdocs ]]; then
        rm -rf $BUILD_PATH/htdocs
      fi
      # do we have the profile?
      if [[ -d $BUILD_PATH/cod_profile ]]; then
        if [[ -d $BUILD_PATH/repos ]]; then
          rm -f /tmp/events.tar.gz
          drush make --prepare-install --no-cache --tar $BUILD_PATH/events.make/events.drupal.org.make /tmp/events
        else
          mkdir -p $BUILD_PATH/repos/modules/contrib
          cd $BUILD_PATH/repos/modules/contrib
          for i in "${modules[@]}"; do
            echo "bringing in ${i} for $USERNAME";
            if [[ -n $USERNAME ]]; then
              git clone ${USERNAME}@git.drupal.org:project/${i}.git
            else
              git clone http://git.drupal.org/project/${i}.git
            fi
          done
          cd $BUILD_PATH/repos
          mkdir -p $BUILD_PATH/repos/themes/contrib
          cd $BUILD_PATH/repos/themes
          for i in "${themes[@]}"; do
            if [[ -n $USERNAME ]]; then
              git clone ${USERNAME}@git.drupal.org:project/${i}.git
            else
              git clone http://git.drupal.org/project/${i}.git
            fi
          done
          build_distro $BUILD_PATH
        fi

        ## extract events site, cod profile and modules
        if [ -e $BUILD_PATH/repos.txt ]; then
          echo "Found a repos text, continuing..."
        else
          cd $BUILD_PATH/repos
          find * -mindepth 1 -maxdepth 2 -type d -not -path ".*" -not -path "modules/.*" -not -path "themes/.*" -not -path "modules/contrib" -not -path "themes/contrib" > $BUILD_PATH/repos.txt
        fi
        # exclude repos since we're updating already by linking it to the repos directory.
        UNTAR="tar -C htdocs --strip-components=1 -zxvf /tmp/events.tar.gz -X $BUILD_PATH/repos.txt"
        mkdir $BUILD_PATH/htdocs
        eval $UNTAR

        # symlink the profile sites folder to our dev copy
        rsync -av $BUILD_PATH/events.make/static-files/ $BUILD_PATH/htdocs/
        cd $BUILD_PATH/htdocs
        ln -s $BUILD_PATH/files $BUILD_PATH/htdocs/sites/default/files
        chmod -R 777 $BUILD_PATH/htdocs/sites/default
        cd $BUILD_PATH/htdocs/profiles/cod
        # We really don't need to symlink cod_profile for end sites. Uncomment if you're developing on the profile itself.
        #for line in $(cat $BUILD_PATH/cod_profile/MANIFEST.txt); do
        #  ln -s ../../../cod_profile/${line} ${BUILD_PATH}/htdocs/profiles/cod/${line}
        #done
        #ln -sf ../../../../cod_profile/themes/cod ${BUILD_PATH}/htdocs/profiles/cod/themes/
        for line in $(cat $BUILD_PATH/repos.txt); do
          if [[ $(echo ${line} | awk -F/ '{print $1}') == 'sites' ]]; then
            ln -s $BUILD_PATH/repos$(echo ${line} | awk -Fsites/all '{print $2}') ${BUILD_PATH}/htdocs/$(echo ${line} | awk -F/ '{print $0}')
          else
            ln -s $BUILD_PATH/repos/${line} ${BUILD_PATH}/htdocs/profiles/cod/$(echo ${line} | awk -F/ '{print $1}')/contrib/
          fi
        done
        chmod -R 775 $BUILD_PATH/htdocs/profiles/cod
      else
        if [[ -n $USERNAME ]]; then
          git clone --branch 7.x-1.x ${USERNAME}@git.drupal.org:project/cod.git cod_profile
        else
          git clone --branch 7.x-1.x http://git.drupal.org/project/cod.git cod_profile
        fi
        build_distro $BUILD_PATH
      fi
  else
    mkdir $BUILD_PATH
    build_distro $BUILD_PATH $USERNAME
  fi
}

site_install() {
  read -p "You're about to DESTROY all data for site ${SITE} Are you sure? " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    cd ${BUILD_PATH}/htdocs/sites/${SITE}
    drush -y sql-drop
    drush site-install --site-name=${SITE} --account-name=admin --account-pass=${ADMIN_PASS} --account-mail=${ADMIN_EMAIL} --site-mail=cod_site@example.com -v -y cod cod_anonymous_welcome_text_form.cod_install_example_content=${DEMO_CONTENT} cod_anonymous_welcome_text_form.cod_anonymous_welcome_title="cod Example Site" cod_anonymous_welcome_text_form.cod_anonymous_welcome_body="Using the site-install version of cod." cod_create_first_group.cod_first_group_title="Sales Group" cod_create_first_group.cod_first_group_body="This is the sales group from site-install."
  fi
}

case $1 in
  site-install)
    if [[ -n $2 ]] && [[ -n $3 ]]; then
      BUILD_PATH=$2
    else
      echo "Usage build_distro.sh site-install [build_path] [site] [demo-content] [admin-email] [admin-pass]"
    fi
    if [[ -n $3 ]]; then
      SITE=$3
    else
      SITE='default'
    fi
    if [[ -n $4 ]]; then
      DEMO_CONTENT='TRUE'
    else
      DEMO_CONTENT='FALSE'
    fi
    if [[ -n $5 ]]; then
      ADMIN_EMAIL=$5
    else
      ADMIN_EMAIL='admin@example.com'
    fi
    if [[ -n $6 ]]; then
      ADMIN_PASS=$6
    else
      ADMIN_PASS='admin'
    fi

    site_install $BUILD_PATH $SITE $DEMO_CONTENT $ADMIN_EMAIL $ADMIN_PASS;;
  pull)
    if [[ -n $2 ]]; then
      BUILD_PATH=$2
      if [[ -n $3 ]]; then
       RESET=1
      fi
    else
      echo "Usage: build_distro.sh pull [build_path]"
      exit 1
    fi
    pull_git $BUILD_PATH $RESET;;
  build)
    if [[ -n $2 ]]; then
      BUILD_PATH=$2
    else
      echo "Usage: build_distro.sh build [build_path]"
      exit 1
    fi
    if [[ -n $3 ]]; then
      USERNAME=$3
    fi
    build_distro $BUILD_PATH $USERNAME;;
  rn)
    if [[ -n $2 ]] && [[ -n $3 ]] && [[ -n $4 ]] && [[ -n $5 ]]; then
      BUILD_PATH=$2
      RELEASE=$3
      FROM_DATE=$4
      TO_DATE=$5
    else
      echo "Usage: build_distro.sh rn [build_path] [release] [from_date] [to_date]"
      exit 1
    fi
    release_notes $BUILD_PATH $RELEASE $FROM_DATE $TO_DATE;;
  update)
    if [[ -n $2 ]]; then
      DOCROOT=$2
    else
      echo "Usage: build_distro.sh update [DOCROOT]"
      exit 1
    fi
    if [[ -n $3 ]]; then
      USERNAME=$3
    fi
    update $DOCROOT;;
  merge_repos)
    if [[ -n $2 ]]; then
      BUILD_PATH=$2
    else
      echo "Usage: build_distro.sh build [build_path]"
      exit 1
    fi
    if [[ -n $3 ]]; then
      USERNAME=$3
    fi
    merge_repos $BUILD_PATH $USERNAME;;
esac
