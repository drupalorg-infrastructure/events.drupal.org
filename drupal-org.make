6api = 2
core = 7.x

; Contributed modules.
projects[bg_image_formatter][type] = "module"
projects[bg_image_formatter][subdir] = "contrib"
projects[bg_image_formatter][version] = "2.0-beta3"

projects[cache_consistent][type] = "module"
projects[cache_consistent][subdir] = "contrib"
projects[cache_consistent][version] = "1.2"
; https://www.drupal.org/node/1679344#comment-10482360
projects[drupal][patch][] = "https://www.drupal.org/files/issues/drupal-7.41-transactional-cache.patch"

projects[charts][type] = "module"
projects[charts][subdir] = "contrib"
projects[charts][version] = "2.0-rc1"

projects[cod_visa_letters][type] = "module"
projects[cod_visa_letters][subdir] = "contrib"
projects[cod_visa_letters][download][type] = "git"
projects[cod_visa_letters][download][url] = "https://git.drupal.org/project/cod_visa_letters.git"
projects[cod_visa_letters][download][branch] = "7.x-2.x"
projects[cod_visa_letters][download][revision] = "35c83c1d4eb022ffa3186a208fb63a80efa9283b"

projects[commerce_cart_expiration][type] = "module"
projects[commerce_cart_expiration][subdir] = "contrib"
projects[commerce_cart_expiration][version] = "1.1"

projects[commerce_eu_vat][type] = "module"
projects[commerce_eu_vat][subdir] = "contrib"
projects[commerce_eu_vat][version] = "2.4"

projects[commerce_india_vat][type] = "module"
projects[commerce_india_vat][subdir] = "contrib"
projects[commerce_india_vat][download][type] = "git"
projects[commerce_india_vat][download][url] = "https://git.drupal.org/project/commerce_india_vat.git"
projects[commerce_india_vat][download][branch] = "7.x-1.x"
projects[commerce_india_vat][download][revision] = "5d695e60af307e383033e74454c8f53d2ccf9adb"

projects[commerce_paypal][type] = "module"
projects[commerce_paypal][subdir] = "contrib"
projects[commerce_paypal][version] = "2.3"

projects[commerce_authnet][subdir] = "contrib"
projects[commerce_authnet][version] = "1.6"

projects[commerce_cheque][type] = "module"
projects[commerce_cheque][subdir] = "contrib"
projects[commerce_cheque][version] = "1.1"

projects[commerce_cybersource_sasop][type] = "module"
projects[commerce_cybersource_sasop][subdir] = "contrib"
projects[commerce_cybersource_sasop][version] = "1.x-dev"
projects[commerce_cybersource_sasop][download][type] = "git"
projects[commerce_cybersource_sasop][download][url] = "https://git.drupal.org/project/commerce_cybersource_sasop.git"
projects[commerce_cybersource_sasop][download][branch] = "7.x-1.x"
projects[commerce_cybersource_sasop][download][revision] = "9f61f512699c6917824a0373aabe9d1f5ad42eeb"

projects[commerce_ogone][type] = "module"
projects[commerce_ogone][subdir] = "contrib"
projects[commerce_ogone][version] = "1.5"

projects[commerce_price_components][type] = "module"
projects[commerce_price_components][subdir] = "contrib"
projects[commerce_price_components][version] = "1.x-dev"

; Discounts aren't shown as components.
; https://www.drupal.org/node/2506585#comment-10026785 for more info.
projects[commerce_price_components][patch][] = "https://www.drupal.org/files/issues/2506585-discounts_arent_shown-1.patch"

projects[commerce_reports][type] = "module"
projects[commerce_reports][subdir] = "contrib"
projects[commerce_reports][version] = "4.0-beta3"

projects[commerce_vat][type] = "module"
projects[commerce_vat][subdir] = "contrib"
projects[commerce_vat][version] = "1.0-rc2"

projects[dfp][type] = "module"
projects[dfp][subdir] = "contrib"
projects[dfp][version] = "1.x-dev"
; Dev version needed for https://www.drupal.org/project/dfp/issues/2825725
projects[dfp][download][type] = "git"
projects[dfp][download][url] = "https://git.drupal.org/project/dfp.git"
projects[dfp][download][branch] = "7.x-1.x"
projects[dfp][download][revision] = "32645bb"

projects[ds][type] = "module"
projects[ds][subdir] = "contrib"
projects[ds][version] = "2.15"

projects[geckoboardapi][type] = "module"
projects[geckoboardapi][subdir] = "contrib"
projects[geckoboardapi][version] = "1.0"

projects[google_analytics][type] = "module"
projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "2.4"

projects[google_tag][subdir] = "contrib"
projects[google_tag][version] = "2.1"

projects[features_extra][type] = "module"
projects[features_extra][subdir] = "contrib"
projects[features_extra][version] = "1.0"

projects[fieldable_panels_panes][type] = "module"
projects[fieldable_panels_panes][subdir] = "contrib"
projects[fieldable_panels_panes][version] = "1.11"

projects[fitvids][type] = "module"
projects[fitvids][subdir] = "contrib"
projects[fitvids][version] = "1.15"

projects[filter_html_image_secure][type] = "module"
projects[filter_html_image_secure][subdir] = "contrib"
projects[filter_html_image_secure][version] = "1.1-beta1"

projects[logintoboggan][subdir] = "contrib"
projects[logintoboggan][version] = "1.5"
; Remove an error caused by logintoboggan's permission js on field permissions
projects[logintoboggan][patch][] = "https://drupal.org/files/issues/logintoboggan-1365764-41.patch"

projects[mailsystem][type] = "module"
projects[mailsystem][subdir] = "contrib"
projects[mailsystem][version] = "2.34"

projects[metatag][version] = "1.25"
projects[metatag][subdir] = "contrib"

projects[mimemail][type] = "module"
projects[mimemail][subdir] = "contrib"
projects[mimemail][version] = "1.1"
; Notice: Undefined offset: 0 in mimemail_html_body()
; https://www.drupal.org/project/mimemail/issues/2883745#comment-13079985
projects[mimemail][patch][] = "https://www.drupal.org/files/issues/2019-04-23/mimemail-plain-text-empty-2883745-42-added-tests.patch"

projects[navbar][type] = "module"
projects[navbar][subdir] = "contrib"
projects[navbar][version] = "1.8"

projects[node_clone][subdir] = "contrib"
projects[node_clone][version] = "1.0-rc2"

projects[honeypot][type] = "module"
projects[honeypot][subdir] = "contrib"
projects[honeypot][version] = "1.24"

projects[scheduler][type] = "module"
projects[scheduler][subdir] = "contrib"
projects[scheduler][version] = "1.5"

projects[slick][type] = "module"
projects[slick][subdir] = "contrib"
projects[slick][version] = "2.0"
projects[slick][patch][] = "https://www.drupal.org/files/issues/2021-03-18/slick-mousewheel-3204431-2.patch"

projects[slick_views][type] = "module"
projects[slick_views][subdir] = "contrib"
projects[slick_views][version] = "2.0"

projects[video_embed_field][type] = "module"
projects[video_embed_field][subdir] = "contrib"
projects[video_embed_field][version] = "2.0-beta11"

projects[views_selective_filters][type] = "module"
projects[views_selective_filters][subdir] = "contrib"
projects[views_selective_filters][version] = "1.6"

; Download the COD install profile and recursively build all its dependencies.
projects[cod][type] = "profile"
;projects[cod][version] = "2.0-rc3"
projects[cod][download][type] = "git"
projects[cod][download][url] = "https://git.drupal.org/project/cod.git"
projects[cod][download][branch] = "7.x-2.x"
projects[cod][download][revision] = "ddee458"
projects[cod][patch][] = "https://bitbucket.org/drupalorg-infrastructure/events.drupal.org/raw/7.x-prod/patches/da-drupal-org-make.patch"

; Custom modules
projects[drupalorg_drupalcon][subdir] = "contrib"
projects[drupalorg_drupalcon][download][type] = "git"
projects[drupalorg_drupalcon][download][revision] = "5c96b07b60a"

; DrupalCon Conference Themes
projects[da_events][type] = "theme"
projects[da_events][download][type] = "git"
projects[da_events][download][url] = "git@bitbucket.org:/drupalorg-infrastructure/events.drupal.org-theme.git"
projects[da_events][download][branch] = "7.x-1.x"
projects[da_events][download][revision] = "4efc85f"

; Asynchronous Prefetch Database Query Cache
projects[apdqc][type] = "module"
projects[apdqc][subdir] = "contrib"
projects[apdqc][version] = "1.0-rc3"
; APDQC probably causing sessions table to grow huge
; https://www.drupal.org/node/2805833#comment-11664393
projects[apdqc][patch][] = "https://www.drupal.org/files/issues/2805833.diff"

; Common for Drupal.org D7 sites.
includes[drupalorg_common_contrib] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/raw/7.x/contrib_path.make"
includes[drupalorg_common] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/raw/7.x/drupal.org-common.make"

; Fitvids Libraries API
libraries[fitvids][download][type] = "get"
libraries[fitvids][download][url] = "https://raw.github.com/davatron5000/FitVids.js/master/jquery.fitvids.js"
libraries[fitvids][directory_name] = "fitvids"
libraries[fitvids][destination] = "libraries"

; Isotope Library
libraries[fitvids][download][type] = "get"
libraries[fitvids][download][url] = "https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.0/isotope.pkgd.min.js"
libraries[fitvids][directory_name] = "isotope"
libraries[fitvids][destination] = "libraries"

; Slick Library
libraries[slick][download][type] = "get"
libraries[slick][download][url] = "https://github.com/kenwheeler/slick/archive/1.5.9.zip"
libraries[slick][directory_name] = "slick"
libraries[slick][destination] = "libraries"
