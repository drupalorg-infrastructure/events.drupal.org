; This file is an accumulation of the drupal-org and drupal-org-core make files to build locally.
core = 7.x
api = 2

; Include the definition of how to build Drupal core directly, including patches.
includes[] = "drupal-org-core.make"
includes[] = "drupal-org.make"
