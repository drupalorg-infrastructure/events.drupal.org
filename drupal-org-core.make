api = 2
core = 7.x

; Include the definition of how to build Drupal core directly, including patches.
includes[] = "https://git.drupalcode.org/project/cod/raw/7.x-2.x/drupal-org-core.make"

; https://www.drupal.org/node/1697570#comment-7940173: _menu_load_objects() is not always called when building menu trees
projects[drupal][patch][] = "https://www.drupal.org/files/drupal7.menu-system.1697570-29.patch"
