<?php

/**
 * Put private settings, like $databases, in settings.local.php.
 *
 * Drupal.org-wide settings, like Bakery, go in /var/www/settings.common.php.
 */

$update_free_access = FALSE;
$drupal_hash_salt = '';

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);

$conf['404_fast_paths_exclude'] = '/\/(?:styles)\//';
$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';


// Disable poormanscron.
$conf['cron_safe_threshold'] = 0;

// We don't use link theming.
$conf['theme_link'] = FALSE;

// Drupal.org settings.
$conf['drupalorg_site'] = 'events';
$conf['drupalorg_navigation'] = FALSE;
// todo uncomment when all permissions are in Features
// $conf['drupalorg_crosssite_lock_permissions'] = TRUE;
$conf['logintoboggan_pre_auth_role'] = 8;
$conf['user_email_verification'] = FALSE;
$conf['drupalorg_crosssite_trusted_role'] = 9;
$conf['drupalorg_crosssite_community_role'] = 10;

// Set proper temporary path - on NFS, outside of http-servable area.
$conf['file_temporary_path'] = '../files-tmp';

// Increase memory limit for Drush.
if (defined('DRUSH_SUCCESS')) {
  ini_set('memory_limit', '800M');
}

// APDQC settings
$conf['cache_backends'][] = 'sites/all/modules/contrib/apdqc/apdqc.cache.inc';
$conf['cache_default_class'] = 'APDQCache';
$conf['cache_class_cache_block'] = 'APDQCache';
$conf['cache_class_cache_form'] = 'APDQCache';
$conf['cache_class_cache_field'] = 'APDQCache';
$conf['cache_class_cache_views'] = 'APDQCache';
$conf['lock_inc'] = 'sites/all/modules/contrib/apdqc/apdqc.lock.inc';
$conf['session_inc'] = 'sites/all/modules/contrib/apdqc/apdqc.session.inc';

// 🇪🇺
$conf['drupalorg_crosssite_gdpr_copy'] = [
  'message' => 'Can we use first and third party cookies and web beacons to <a href="https://www.drupal.org/terms">understand our audience, and to tailor promotions you see</a>?',
  'yes' => 'Yes, please',
  'no' => 'No, do not track me',
  'status' => 'You are currently <strong class="opted-in">opted in</strong><strong class="opted-out">opted out</strong>.',
];

/**
 * Include a common settings file if it exists.
 */
$common_settings = '/var/www/settings.common.php';
if (file_exists($common_settings)) {
  include $common_settings;
}

/**
 * Include a local settings file if it exists.
 */
$local_settings = dirname(__FILE__) . '/settings.local.php';
if (file_exists($local_settings)) {
  include $local_settings;
}
